#include "Kolejka.hpp"
#include "Kolejka_priorytetowa.hpp"
#include "Lista.hpp"
#include "Stos.hpp"
#include "Tablica.hpp"
#include <iostream>

#include <stack>
#include <queue>
#include <list>

int main()
{
	std::cout<<"Kolejka:"<<std::endl;
	ciuchciuch::Testy();
	std::cout<<"Kolejka priorytetowa:"<<std::endl;
	prio::Testy();
	std::cout<<"Lista:"<<std::endl;
	ciastko::Testy();
	std::cout<<"Stos:"<<std::endl;
	salem::Testy();
	std::cout<<"Tablica:"<<std::endl;
	beep::Testy();
	
	
	std::cout<<"STL:"<<std::endl;
	
	std::cout<<"Stos:"<<std::endl;
	std::stack<int> stos;
	stos.push(28);
	stos.push(15);
	stos.push(3);
	std::cout<<stos.top()<<' ';
	stos.pop();
	std::cout<<stos.top()<<' ';
	stos.pop();
	std::cout<<stos.top()<<std::endl;
	
	std::cout<<"Kolejka:"<<std::endl;
	std::queue<int> kolejka;
	kolejka.push(5);
	kolejka.push(10);
	kolejka.push(15);
	std::cout<<kolejka.front()<<' ';
	kolejka.pop();
	std::cout<<kolejka.front()<<' ';
	kolejka.pop();
	std::cout<<kolejka.front()<<std::endl;
	
	std::cout<<"Kolejka priorytetowa:"<<std::endl;
	std::priority_queue < int > priori;
	priori.push(5);
	priori.push(9);
	priori.push(6);
	std::cout<<priori.top()<<' ';
	priori.pop();
	std::cout<<priori.top()<<' ';
	priori.pop();
	std::cout<<priori.top()<<std::endl;
	
	std::cout<<"Lista:"<<std::endl;
	std::list<int> lista;
	lista.push_back(3);
	lista.push_back(8);
	lista.push_front(12);
	std::cout<<lista.size()<<' ';
	
	
	std::cout<<"Tablica:"<<std::endl;
	


	
	
	return 0;
}
