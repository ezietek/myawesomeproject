#ifndef STOS
#define STOS

#include <iostream>


namespace salem	//po zakonczeniu namespacea nie daje sie sirednika!!!!!!
{
	template<typename T>
	class Wiedzma	// obiekt na stosie
	{
		protected:
			
		public:
			T wartosc;
			Wiedzma<T>* poprzedni; // wskaznik na poprzedni element
	};
	
	/*Wiedzma<T>::Wiedzma<T>()	// dlaczego to nie dziala????????????????????????????????????
	{
		this->poprzedni = nullptr;
	};*/
	
	template<typename T>
	class Stos
	{
		friend class Wiedzma<T>;
		
		protected:
			Wiedzma<T>* gora;	// wskaznik na pierwszy element stosu
			int liczba_kobiet_na_stosie;
			
		public:
			Stos(){ gora = NULL; liczba_kobiet_na_stosie = 0;};
			~Stos(){Podpal();};
			
			void Push(T kobieta)	// element - nowy klocek
			{
				Wiedzma<T> *element = (new Wiedzma<T>);
				element->wartosc = kobieta;
				element->poprzedni = gora;	// przekazuje wskaznik na stary klocek nowemu
				gora = element;			// zapisuje nowy wskaznik na gore stosu
				liczba_kobiet_na_stosie++;
			}
			
			T Pop()
			{
				if(this->liczba_kobiet_na_stosie > 0)
				{
					Wiedzma<T>* kopia = this->gora;
					T jednak_nie_wiedzma = this->gora->wartosc;
					this->gora = this->gora->poprzedni;
					delete kopia;
					this->liczba_kobiet_na_stosie--;
					return jednak_nie_wiedzma;
				}
				T pomylka = -9999;
				return pomylka;
			}
			
			void Podpal()
			{
				while(this->liczba_kobiet_na_stosie > 0)
				{
					Pop();
				}
			}
	};
	
	void Testy()
	{
		salem::Stos<int> plon;
		plon.Push(1);
		plon.Push(2);
		plon.Push(3);
		std::cout<<plon.Pop()<<std::endl<<plon.Pop()<<std::endl<<plon.Pop()<<std::endl;
		
	}
}

#endif
