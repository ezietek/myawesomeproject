#ifndef KOLEJKA_PRIORYTETOWA
#define KOLEJKA_PRIORYTETOWA

#include <iostream>
using namespace std;

namespace prio
{
	template<typename T>
	class Klient
	{
		public:
			Klient<T>* nastepny;
			T wartosc;
			int kalectwo;
			Klient(){ nastepny = NULL; wartosc = 0; kalectwo = 0;};
	};
	
	template<typename T>
	class Kolejka
	{
		friend class Klient<T>;
		
		protected:
			Klient<T>* head;
			Klient<T>* tail;
			
			
		public:
		
		int dlugosc_kolejki;
		
			Kolejka(){ head = NULL; tail = NULL; dlugosc_kolejki = 0;};
			~Kolejka(){Zamknij_sklep();};
			
			void Push(T customer, int ciezarna)	// element - nowy klocek
			{
				/*Klient<T> *element = (new Klient<T>);
				element->wartosc = customer;
				
				
				if(head == NULL)
				{
					head = element;
					tail = element;
				}
				
				else
				{
					tail->nastepny = element;
					tail = element;
					
				}*/
				
				Klient<T> *element = (new Klient<T>);
				Klient<T> *pamietaj = (new Klient<T>);
				
				element->wartosc = customer;
				element->kalectwo = ciezarna;
				
				if(head == NULL)		// jesli kolejka jest pusta, ustawia pierwszy element
				{
					head = element;
					tail = element;
				}
				
				else if(head->kalectwo >= ciezarna)		// sprawdza, czy priorytet jest wiekszy od obecnego head'a. Jesli jest to dodaje nowego
				{
					element->nastepny = element;
					head = element;
				}
				
				else
				{
					pamietaj = head;	// przeszukuje kolejke, czy ktorys element nie ma wyzszego priorytetu i wstawia element przed niego
										// wyzszy priorytet - wyzszy numer
					
					while((pamietaj->nastepny != NULL) && (pamietaj->nastepny->kalectwo >= ciezarna))
					{
						pamietaj = pamietaj->nastepny;
					}
					
					element->nastepny = pamietaj->nastepny;
					pamietaj->nastepny = element;
					
				}
				
				if(element->nastepny == NULL)
					{
						tail = element;
					}
				
				
				dlugosc_kolejki++;
				
			}
			
			T Pop()
			{
				if(this->head)
				{
					Klient<T>* kopia = this->head;
					T obsluzony = this->head->wartosc;
						this->head = this->head->nastepny;
					if (head == 0) tail = 0;
					delete kopia;
					this->dlugosc_kolejki--;
					
					
					return obsluzony;
				}
				
				
				T pomylka = -9999;
				return pomylka;
			}
			
			T Front()				// podglada pierwsza wartosc w kolejce
			{
				if(this->head)
				{
					T obsluzony = this->head->wartosc;
					return obsluzony;
				}
				T pomylka = -9991;
				return pomylka;
			}
			
			void Zamknij_sklep()	// usuwa wszystkie dane w kolejce
			{
				while(this->dlugosc_kolejki > 0)
				{
					Pop();
				}
			}
	};
	
	void Testy()
	{
		prio::Kolejka<int> tgw;
		tgw.Push(10, 10);
		tgw.Push(20, 1);
		tgw.Push(3, 3);
		std::cout<<tgw.Pop() << std::endl;
		std::cout<<tgw.Pop() << std::endl;
		std::cout<<tgw.Pop() << std::endl;
	}
	
}

#endif
