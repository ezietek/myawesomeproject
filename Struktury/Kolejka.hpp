#ifndef KOLEJKA
#define KOLEJKA


#include <iostream>
using namespace std;

namespace ciuchciuch
{
	template<typename T>
	class Klient
	{
		public:
			Klient<T>* nastepny;
			T wartosc;
			Klient(){ nastepny = NULL; wartosc = 0;};
	};
	
	template<typename T>
	class Kolejka
	{
		friend class Klient<T>;
		
		protected:
			Klient<T>* head;
			Klient<T>* tail;
			int dlugosc_kolejki;
			
		public:
		
		
		
			Kolejka(){ head = NULL; tail = NULL; dlugosc_kolejki = 0;};
			~Kolejka(){Zamknij_sklep();};
			
			void Push(T customer)	// element - nowy klocek
			{
				Klient<T> *element = (new Klient<T>);
				element->wartosc = customer;
				
				
				if(head == NULL)
				{
					head = element;
					tail = element;
				}
				
				else
				{
					tail->nastepny = element;
					tail = element;
					
				}
				
				dlugosc_kolejki++;
				
			}
			
			T Pop()
			{
				if(this->head)
				{
					Klient<T>* kopia = this->head;
					T obsluzony = this->head->wartosc;
						this->head = this->head->nastepny;
					if (head == 0) tail = 0;
					delete kopia;
					this->dlugosc_kolejki--;
					
					
					return obsluzony;
				}
				
				
				T pomylka = -9999;
				return pomylka;
			}
			
			T Front()				// podglada pierwsza wartosc w kolejce
			{
				if(this->head)
				{
					T obsluzony = this->head->wartosc;
					return obsluzony;
				}
				T pomylka = -9991;
				return pomylka;
			}
			
			void Zamknij_sklep()	// usuwa wszystkie dane w kolejce
			{
				while(this->dlugosc_kolejki > 0)
				{
					Pop();
				}
			}
	};
	
	void Testy()
	{
		ciuchciuch::Kolejka<int> tgw;
		tgw.Push(10);
		tgw.Push(20);
		tgw.Push(3);
		std::cout<<tgw.Pop() << std::endl;
		std::cout<<tgw.Pop() << std::endl;
		std::cout<<tgw.Pop() << std::endl;
	}
	
}
#endif
