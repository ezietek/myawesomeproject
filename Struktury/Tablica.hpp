#ifndef TABLICA
#define TABLICA

#include <iostream>
#include <string>
#include <numeric>

namespace beep
{
	template<typename T>
	class Komorka
	{
		T wartosc;
		
		public:
			Komorka(){wartosc = 0;};
			T& Dostep(){return wartosc;};
	};
	
	template<typename K, typename T>
	class Tablica
	{
		Komorka<T> table[10];
		public:
			
			T& operator [] (K klucz)
			{
				return table[Koduj(klucz)].Dostep();
			}
			
			void Insert(K klucz, T dane)
			{
				int temp = 0;
				temp = Koduj(klucz);
				table[temp].Dostep() = dane;
				
			}
			
			void Usun(K klucz)
			{
				int temp = 0;
				temp = Koduj(klucz);
				table[temp].Dostep() = 0;
				
			}
			
			int Koduj(K klucz)
			{
				int wyjscie = 0;
				wyjscie = std::accumulate(klucz.begin(), klucz.end(), 1);
				wyjscie = wyjscie % 10;
				
				return wyjscie;
			}
		 
	};
	
	void Testy()
	{
		beep::Tablica<std::string, int> boop;
		boop.Insert("ptaszek", 3);
		boop.Insert("motylek", 10);
		boop.Insert("a", 100);
		
		std::cout << boop["a"]<< std::endl;
		std::cout << boop["motylek"]<< std::endl;
		std::cout << boop["ptaszek"]<< std::endl;
		//std::cout << "ser" << std::endl;
	}
}
	
#endif
