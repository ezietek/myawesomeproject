#ifndef LISTA
#define LISTA

#include <iostream>

namespace ciastko
{
	template<typename T>
	class Klocek
	{
		public:
			Klocek<T>* nastepny;
			Klocek<T>* poprzedni;
			T wartosc;
			Klocek(){ nastepny = NULL; poprzedni = NULL; wartosc = 0;};
	};
	
	template<typename T>
	class Lista
	{
		private:
			Klocek<T>* head;
			Klocek<T>* tail;
			int ilosc_klockow;
			
		public:
			Lista(){ head = NULL; tail = NULL; ilosc_klockow = 0;};
			~Lista(){;};
			
			
			
			void Push_na_przod(T val)
			{
				Klocek<T> *element = (new Klocek<T>);
				element->wartosc = val;
				
				if(head == NULL)		// jesli lista jest pusta, ustawia pierwszy element
				{
					head = element;
					tail = element;
				}
				
				else
				{
					element->poprzedni = head;	// przekazuje wskaznik na stary klocek nowemu
					head->nastepny = element;	// przekazuje nowemu klockowi wskaznik na stary
					head = element;				// przesuwa wskaznik na poczatek
				}
				
				ilosc_klockow++;
			}
			
			
			
			void Push_na_koniec(T val)
			{
				Klocek<T> *element = (new Klocek<T>);
				element->wartosc = val;
				
				if(head == NULL)
				{
					head = element;
					tail = element;
				}
				
				else
				{
					tail->poprzedni = element;	// dodaje nowy element
					element->nastepny = tail;
					tail = element;				// przesuwa wskaznik na koniec
				}
				
				ilosc_klockow++;
				
			}
			
			
			/*** Podaj numer od poczatku listy ***/
			void Push_przed(T val, int numer)
			{
				Klocek<T> *pomoc = (new Klocek<T>);
				pomoc = head;
				
				for(int i = 1; i<numer; i++)	// znajduje element, przed ktory chcemy wrzucic nowy klocek
				{
					pomoc = pomoc->poprzedni;
				}
				
				if(pomoc == head)
				{
					Push_na_przod(val);
				}
				
				else
				{
					Klocek<T> *element = (new Klocek<T>);
					element->wartosc = val;
					
					element->poprzedni = pomoc;		// nowy klocek wskazuje klocek wczesniejszy
					element->nastepny = pomoc->nastepny;	// nowy klocek wskazuje element pozniejszy
					pomoc->nastepny->poprzedni = element;	// stary klocek, ktory byl przed wybranym wskazuje teraz na nowy
					pomoc->nastepny = element;		// wybrany klocek wskazuje teraz na nowy
					
				}
				
				ilosc_klockow++;
			}
			
			
			/*** Podaj numer od poczatku listy ***/
			void Push_za(T val, int numer)
			{
				Klocek<T> *pomoc = (new Klocek<T>);
				pomoc = head;
				
				for(int i = 1; i<numer; i++)	// znajduje element, przed ktory chcemy wrzucic nowy klocek
				{
					pomoc = pomoc->nastepny;
				}
				
				if(pomoc == tail)
				{
					Push_na_koniec(val);
				}
				
				else
				{
					Klocek<T> *element = (new Klocek<T>);
					element->wartosc = val;
					
					element->poprzedni = pomoc->poprzedni;		// nowy klocek wskazuje klocek wczesniejszy
					element->nastepny = pomoc;	// nowy klocek wskazuje element pozniejszy
					pomoc->poprzedni->nastepny = element;	// stary klocek, ktory byl przed wybranym wskazuje teraz na nowy
					pomoc->poprzedni = element;		// wybrany klocek wskazuje teraz na nowy
					
				}
				
				ilosc_klockow++;
			}
			
			
			
			T Pop_przod()
			{
				if(this->head)
				{
					Klocek<T>* kopia = this->head;
					T obsluzony = this->head->wartosc;
						this->head = this->head->poprzedni;
						this->head->nastepny = NULL;
					if (head == 0) tail = 0;
					delete kopia;
					this->ilosc_klockow--;
					
					
					return obsluzony;
				}
				
				
				T pomylka = -9999;
				return pomylka;
				
			}
			
			
			
			T Pop_koniec()
			{
				if(this->tail)
				{
					Klocek<T>* kopia = this->tail;
					T obsluzony = this->tail->wartosc;
						this->tail = this->tail->nastepny;
						this->tail->poprzedni = NULL;
					if (tail == 0) head = 0;
					delete kopia;
					this->ilosc_klockow--;
					
					
					return obsluzony;
				}
				
				
				T pomylka = -9999;
				return pomylka;
				
			}
			
			
			/*** Podaj numer od poczatku listy ***/
			T Pop(int numer)
			{
				Klocek<T> *pomoc = (new Klocek<T>);
				T obsluzony;
				pomoc = head;
				
				for(int i = 1; i<numer; i++)	// znajduje element, przed ktory chcemy wrzucic nowy klocek
				{
					pomoc = pomoc->nastepny;
				}
				
				if(pomoc == head)
				{
					obsluzony = Pop_przod();
					ilosc_klockow--;
					return obsluzony;
				}
				
				else if(pomoc == tail)
				{
					obsluzony = Pop_koniec();
					ilosc_klockow--;
					return obsluzony;
				}
				
				else
				{
					if(numer <= ilosc_klockow)
					{
						obsluzony = pomoc->wartosc;
						pomoc->nastepny->poprzedni = pomoc->poprzedni;
						pomoc->poprzedni->nastepny = pomoc->nastepny;
						delete pomoc;
						ilosc_klockow--;
						return obsluzony;
					}
					
					else
						obsluzony = 9999;
						return obsluzony;
				}
			}
	};
	
	void Testy()
	{
		ciastko::Lista<int> l;
	l.Push_na_przod(12);
	l.Push_na_przod(9);
	l.Push_na_przod(3);
	//l.Push_na_koniec(27);
	//l.Push_na_koniec(44);
	//l.Push_przed(9, 2);
	//l.Push_za(99, 1);
	//std::cout<<l.Pop(2)<<std::endl;
	std::cout<<l.Pop_przod()<<std::endl;
	std::cout<<l.Pop_przod()<<std::endl;
	//std::cout<<l.Pop_przod()<<std::endl;
	//std::cout<<l.Pop_przod()<<std::endl;
	//std::cout<<l.Pop_przod()<<std::endl;
	//std::cout<<l.Pop_koniec()<<std::endl;
		
	}
}

#endif
