#ifndef STRUKTURY_HPP
#define STRUKTURY_HPP

#include <iostream>
#include <vector>
#include <list>

struct komorka{
	int wierzcholek;
	int waga;
};

class Graf_lista
{
	public:
		const std::list<komorka> & operator [] (int indeks) const {return graf[indeks];};
		std::list<komorka> & operator [] (int indeks) {return graf[indeks];};
		void Resize(int rozmiar){graf.resize(rozmiar);};
		int Rozmiar()const{return graf.size();};
		
	private:
		
		
	protected:
		std::vector<std::list<komorka> > graf;
		
	 
};

std::ostream& operator << (std::ostream& stream, Graf_lista object)
{
	stream << object.Rozmiar();
	for(int i = 0; i < object.Rozmiar(); i++)
	{
		stream << " " << object[i].size();
		for(komorka& Wsk : object[i])
		{
			stream << " " << Wsk.wierzcholek << " " << Wsk.waga;
		}
	}
	return stream;
}

std::istream& operator >> (std::istream& strumien, Graf_lista& cos)
{
	int rozmiar = 0;
	strumien >> rozmiar;
	cos.Resize(rozmiar);
	
	for(int i = 0; i < rozmiar; i++)
	{
		int ilosc_krawedzi_dla_poj_wierzcholka = 0;
		strumien >> ilosc_krawedzi_dla_poj_wierzcholka;
		cos[i].resize(ilosc_krawedzi_dla_poj_wierzcholka);
		
		for(komorka& zmienna : cos[i])  // petla zakresu przeskakuje po kolejnych miejscach w liscie dopoki sie nie skoncza
		{							// nnie wychodzi poza zakres, konczy sie wtedy
			strumien >> zmienna.wierzcholek;	// zmienna jest referencja na element listy
			strumien >> zmienna.waga;	// zmienna jest referencja na element listy
		}
	}
	
	return strumien;
}

class Graf_macierz
{
	public:
		const std::vector<int> & operator [] (int indeks) const {return graf[indeks];};
		std::vector<int> & operator [] (int indeks) {return graf[indeks];};
		int Rozmiar()const{return graf.size();};
		void Resize(int rozmiar)
			{
				graf.resize(rozmiar);
				for(int i = 0; i < rozmiar; i++)
				{
					graf[i].resize(rozmiar);
					
					for(int j = 0; j < rozmiar; j++)
					{
						graf[i][j] = 0;
					}
				}
				
			};
	
	private:
	
	protected:
	
		std::vector<std::vector<int> > graf;
	
};

std::istream& operator >> (std::istream& strumien, Graf_macierz& cos)
{
	int rozmiar = 0;
	strumien >> rozmiar;
	cos.Resize(rozmiar);
	
	for(int i = 0; i < rozmiar; i++)
	{
		int ilosc_krawedzi_dla_poj_wierzcholka = 0;
		strumien >> ilosc_krawedzi_dla_poj_wierzcholka;
		
		for(int j = 0; j < ilosc_krawedzi_dla_poj_wierzcholka; j++)
		{
			int wspolrzedna = 0;
			int waga = 0;
			strumien >> wspolrzedna;
			strumien >> waga;
			cos[i][wspolrzedna] = waga;
		}
		
	}
	
	return strumien;
}


#endif
