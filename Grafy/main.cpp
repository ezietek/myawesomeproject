#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <fstream>
#include "Tester.h"
#include "Tabelka.hpp"
#include "Struktury.hpp"
#include "Algorytmy.hpp"

/**********************************************************
 * 		DANE WCZYTYWANE W FORMACIE
 * 
 *  Laczna ilosc wierzcholkow + (liczba sasiadow danego wierzcholka + (sasiad + waga)*ilosc sasiadow danego wierzcholka)* ilosc wierzcholkow 
 * 
 *  Na wejsciu nalezy podac 100 instancji
 *  W przypadku macierzy droga nie moze wynosic 0
 * 
 *  Przy pisaniu programu wspolpracowalam z Jakubem Grzana.
 *  Kozystam z napisanego przez niego generatora danych wejsciowych i testera.
 * 
 * ********************************************************/


template<typename typ_danych_wejscowych>
class Graf_tester : public Tester<Tabelka, typ_danych_wejscowych>
{
	public:
		Graf_tester(char a){algorytm = a;};
		
	private:
		char algorytm;
		
	protected:
		
		typ_danych_wejscowych readInput(std::istream& input)
		{
			typ_danych_wejscowych element;
			input >> element;
			
			return element;
		}
		
		Tabelka runAlgorithm(const typ_danych_wejscowych& data)
		{
			Tabelka a;
			switch(algorytm)
			{
				case 'd':
					a = Dijkstra(data);
					//std::cout<< a<<std::endl;
					break;
				
				case 'b':
					a = Bellman_Ford(data);
					//std::cout<< a<<std::endl;
					break;
					
				default:
					std::cout<<"Blad. Niewlasciwy algorytm. Wpisz 'd' albo 'b'." << std::endl;
					exit(0);
			}
			
			return a;
		}
};
	
int Obsluga_wejscia(std::ifstream& plik, std::ofstream& wynik, std::ofstream& czas)
{
	std::string nazwa;
	std::cout << "Podaj nazwe pliku" << std::endl;
	std::cin >> nazwa;
	plik.open(nazwa.c_str());
			
	if(!plik.is_open())
	{
		std::cout << "Blad otwarcia pliku" << std::endl;
		return 1;
	}
			
	nazwa = "wynik.txt";
	wynik.open(nazwa.c_str());
	nazwa = "czas.csv";
	czas.open(nazwa.c_str());
	
	return 0;
}

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::cout << "Podaj odpowednie argumenty wywolania" << std::endl;
		std::cout << "1. struktura danych w jakich program zapisze dane (l - lista, m - macierz)" << std::endl;
		std::cout << "2. rodzaj algorytmu ('d' - Dijkstry, 'b'- Bellmana-Forda" << std::endl;
		std::cout << std::endl;
	}
	
	else
	{
		std::ifstream plik;
		std::ofstream wynik;
		std::ofstream czas;
		
		char a = argv[1][0];	// w wywolaniu mozesz wpisac cos wiecej niz char. Argv jest tablica tablic. Tutaj sprawdza pierwsza litere stringu
		
		Obsluga_wejscia(plik, wynik, czas);
		
		switch(a)
		{
			case 'l':
				{
					
					Graf_tester<Graf_lista> testy(argv[2][0]);	// gdy deklaruje sie zmienne w case, musza byc w klamrach, miedzy case, a break
					testy.runTests(plik, wynik, czas);
					
				}
				break;
			
			case 'm':
				{
					Graf_tester<Graf_macierz> testy(argv[2][0]);
					testy.runTests(plik, wynik, czas);
					
				}
				break;
			
			default:
				std::cout<<"Bledny typ danych, wpisz 'l' albo 'm'" << std::endl;	
				break;
		}
		
		
	}
	
	return 0;
}
