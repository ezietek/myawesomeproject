#ifndef ALGORYTMY_HPP
#define ALGORYTMY_HPP

#include "Tabelka.hpp"
#include "Struktury.hpp"
#include <vector>

Tabelka Dijkstra(const Graf_lista& lista)
{
	Tabelka a;
	
	a.Resize(lista.Rozmiar());
	
	int badany_wierzcholek = 0;		//std::vector<komorka_tabelki> tabelka a:		sprawdzone		suma_wag	poprzednik
									//vector<std::list<komorka> lista:zmienna		wierzcholek		waga							
	int pozostalo = a.Rozmiar();
	while(pozostalo!=0)
	{	
		a[badany_wierzcholek].sprawdzone = true;		//std::vector<std::vector<int> > graf
		pozostalo--;
		
		for(const komorka& zmienna : lista[badany_wierzcholek])
		{
				if(a[zmienna.wierzcholek].suma_wag > a[badany_wierzcholek].suma_wag + zmienna.waga)
				{
					a[zmienna.wierzcholek].suma_wag = a[badany_wierzcholek].suma_wag + zmienna.waga;
					a[zmienna.wierzcholek].poprzednik = badany_wierzcholek;
				}
			
		}
		
		unsigned int najmniejsza_wartosc = 999999;
		//unsigned int kolejny_wierzcholek = 0;
			
		for(int i = 0; i < a.Rozmiar(); i++)
		{
				
			if(a[i].sprawdzone == false)
			{
				if(najmniejsza_wartosc > a[i].suma_wag)
				{
					//kolejny_wierzcholek = i;
					badany_wierzcholek = i;
					najmniejsza_wartosc = a[i].suma_wag;
				}
			}	
		}
		
		
	}
	
	return a;
}



Tabelka Dijkstra(const Graf_macierz& macierz)
{
	Tabelka a;
	
	a.Resize(macierz.Rozmiar());
	int badany_wierzcholek = 0;
	int pozostalo = a.Rozmiar();
	
	while(pozostalo!=0)
	{
		a[badany_wierzcholek].sprawdzone = true;		//std::vector<std::vector<int> > graf
		pozostalo--;
		
		for(int indeks = 0; indeks < macierz.Rozmiar(); indeks++)
		{
			if(/*a[indeks].sprawdzone == false &&*/ macierz[badany_wierzcholek][indeks] != 0)
			{
				if(a[indeks].suma_wag > a[badany_wierzcholek].suma_wag + macierz[badany_wierzcholek][indeks])
				{
					a[indeks].suma_wag = a[badany_wierzcholek].suma_wag + macierz[badany_wierzcholek][indeks];
					a[indeks].poprzednik = badany_wierzcholek;
				}
			}
			
		}
		
		unsigned int najmniejsza_wartosc = 999999;
		//unsigned int kolejny_wierzcholek = 0;
			
		for(int i = 0; i < a.Rozmiar(); i++)
		{
				
			if(a[i].sprawdzone == false)
			{
				if(najmniejsza_wartosc > a[i].suma_wag)
				{
					//kolejny_wierzcholek = i;
					badany_wierzcholek = i;
					najmniejsza_wartosc = a[i].suma_wag;
				}
			}	
		}
			
		//badany_wierzcholek = kolejny_wierzcholek;
	
	
	}
	
	return a;
}



Tabelka Bellman_Ford(const Graf_lista& lista)
{
	Tabelka a;
	
	a.Resize(lista.Rozmiar());
	
	bool zmiana = true;
	
	for(int iteracja = 0; iteracja < a.Rozmiar(); iteracja++)
	{
		if(zmiana == true)
		{
			zmiana = false;
			
			for(int badany_wierzcholek = 0; badany_wierzcholek < lista.Rozmiar(); badany_wierzcholek++)
			{	
				for(const komorka& zmienna : lista[badany_wierzcholek])
				{
					if(a[zmienna.wierzcholek].suma_wag > a[badany_wierzcholek].suma_wag + zmienna.waga)
					{
						a[zmienna.wierzcholek].suma_wag = a[badany_wierzcholek].suma_wag + zmienna.waga;
						a[zmienna.wierzcholek].poprzednik = badany_wierzcholek;
						zmiana = true;
					}
					
				}
				
			}
		
		}
		
		else
			break;
	} 
	
	return a;
}



Tabelka Bellman_Ford(const Graf_macierz& macierz)
{
	Tabelka a;
	
	a.Resize(macierz.Rozmiar());
	
	bool zmiana = true;
	
	for(int iteracja = 0; iteracja < a.Rozmiar(); iteracja++)
	{
		if(zmiana == true)
		{
			zmiana = false;
			
			for(int badany_wierzcholek = 0; badany_wierzcholek < macierz.Rozmiar(); badany_wierzcholek++)
			{	
				for(int indeks = 0; indeks < macierz.Rozmiar(); indeks++)
				{
					if(/*a[indeks].sprawdzone == false &&*/ macierz[badany_wierzcholek][indeks] != 0)
					{
						if(a[indeks].suma_wag > a[badany_wierzcholek].suma_wag + macierz[badany_wierzcholek][indeks])
						{
							a[indeks].suma_wag = a[badany_wierzcholek].suma_wag + macierz[badany_wierzcholek][indeks];
							a[indeks].poprzednik = badany_wierzcholek;
							zmiana = true;
						}
					}
					
				}
				
			}
		
		}
		
		else
			break;
	} 
	
	return a;
}


#endif
