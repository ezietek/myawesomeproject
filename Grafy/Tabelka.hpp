#ifndef TABELKA_HPP
#define TABELKA_HPP

#include <iostream>
#include <vector>

struct komorka_tabelki
{
	bool sprawdzone;
	unsigned int suma_wag;
	int poprzednik;
	
	komorka_tabelki()
	{
		sprawdzone = false;
		suma_wag = 999999;
		poprzednik = -1;
	}
};


class Tabelka
{
	public:
		int Rozmiar(){return tabelka.size();};
		void Resize(int rozmiar) {tabelka.resize(rozmiar); tabelka[0].suma_wag = 0;};
		const komorka_tabelki & operator [] (int indeks) const {return tabelka[indeks];};
		komorka_tabelki & operator [] (int indeks) {return tabelka[indeks];};
			
	private:
		std::vector<komorka_tabelki> tabelka;
		
	protected:
		
};

std::ostream& operator << (std::ostream& strumien, Tabelka cos)
{
	strumien << cos.Rozmiar() << std::endl;
	
	for(int i = 0; i < cos.Rozmiar(); i++)
	{
		strumien << cos[i].suma_wag << '\t';
	}

	strumien << std::endl;
	
	for(int i = 0; i < cos.Rozmiar(); i++)
	{
		strumien << cos[i].poprzednik << '\t';
	}
	
	strumien << std::endl;
	
	return strumien;
}

#endif
