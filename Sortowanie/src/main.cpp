#include "pamsi_tester/Tester.hpp"
#include "spdlog/spdlog.h"
#include <fstream>
#include <numeric>
#include <vector>
#include <iostream>

/************************************************
 * Ewa Ziętek 241479
 * 
 * Podczas tworzenia programu współpracowałam z Jakubem Grzaną.
 * Razem układaliśmy algorytmy i rozmyślaliśmy nad implementacją,
 * ale sam kod pisaliśmy oddzielnie.
 * 
 ***********************************************/

void Zamien(int &a, int &b)	// zamienia miejscami elementy tablicy
{
	int c = 0;
	c = a;
	a = b;
	b = c;
}

class Tablica
{
	public:
		Tablica(int a) : tablica(a) {};	// kolejka inicjalizacyjna - wykorzystuje konsruktor vectora, tworzy tablice dlugosci a
		int rozmiar()const{return tablica.size();};
		const int & operator [] (int indeks)const{ return tablica[indeks];};
		int& operator [] (int indeks){ return tablica[indeks];};
		
		//T & operator[](size_t el) {return Tab[el];}
  //const T & operator[](size_t el) const {return Tab[el];}
		
		void Usun_ostatni(){ tablica.pop_back();};
		
		
		Tablica Quicksort()const;
		Tablica Heapsort()const;
		Tablica Bubblesort()const;
		
		
		
		
	private:
		void Jestem_szybki(int lewa, int prawa);
		int Podziel(int lewa, int prawa);
	
		int Buduj_kopiec(int wielkosc);
		//int Sortuj(int wielkosc);
		int Sprawdz_galaz(int i);
		int Ojciec(int i);
		int Lewe(int i);
		int Prawe(int i);
		
		void Bulbul(int wielkosc);
		
	protected:
		std::vector<int> tablica;
	
	
};

std::istream &operator >> (std::istream &strumien, Tablica &dane)
{
	for(int i = 0; i < dane.rozmiar(); i++)
	{
		strumien >> dane[i];
	}
	
	return strumien;
}

std::ostream &operator << (std::ostream &strumien, Tablica dane)
{
	for(int i = 0; i < dane.rozmiar(); i++)
	{
		strumien << dane[i] << ' ';
	}
	
	strumien << std::endl;
	
	return strumien;
}

/********************	QUICKSORT *********************/

Tablica Tablica::Quicksort()const
{
	Tablica s=*this;
	
	std::cout<<"Quicksort"<<std::endl;
	
	s.Jestem_szybki(0, s.rozmiar());
	
	
	return s;
}
// wzorowany na algorytmie z algorytm.org
int Tablica::Podziel(int poczatek, int koniec)	// dzieli tablice na dwie czesci
{
	int klucz = tablica[poczatek];	
	int i = poczatek;
	int j = koniec;
	
	while(true)
	{
		while(tablica[j] > klucz)
		{
			j--;
		}
		
		while(tablica[i] < klucz)
		{
			i++;
		}
		
		if(i < j)
		{
			Zamien(tablica[i], tablica[j]);
			i++;
			j--;
		}
		
		else
		{
			return j;	// zwraca punkt podzialu tablicy
		}
	}
	
	return j;
}

void Tablica::Jestem_szybki(int lewa, int prawa)	// dzieli tablice na dwie czesci i rekurencyjnie wykonuje sortowanie
{
	int q = 0;
	
	if(lewa < prawa)
	{
		q = Podziel(lewa, prawa);
		Jestem_szybki(lewa, q);
		Jestem_szybki(q+1, prawa);
	}
	
}
/*********************************************************/


/*****************		HEAPSORT	**********************/

Tablica Tablica::Heapsort()const
{
	std::cout<<"Heapsort"<<std::endl;
	
	Tablica s=*this;
	Tablica r=*this;
	int nosiciel = 0;
	int glupiakrowa = s.rozmiar();
	
	for(int i = 0; i < glupiakrowa; i++)
	{
		nosiciel = s.Buduj_kopiec(s.rozmiar()-1);
		//std::cout << i << "   " << nosiciel << std::endl;
		r[r.rozmiar()-i-1] = nosiciel;
		s.Usun_ostatni();
	}
	
	return r;
}

/*Tablica Tablica::Sortuj(int wielkosc)
{
	
	
	return 0;
}*/

int Tablica::Buduj_kopiec(int wielkosc)
{	
	//std::cout << wielkosc << std::endl;
	
	for(int i = wielkosc; i >= 0; i--)
	{
		Sprawdz_galaz(i);
	}
	
	Zamien(tablica[0], tablica[wielkosc]);
	
	return tablica[wielkosc];
}

int Tablica::Sprawdz_galaz(int i)
{
	if((Lewe(i) <= rozmiar()-1) && (Prawe(i) <= rozmiar()-1))	// sprawdza czy nie wychodzi poza tablice
	{
		/*if((tablica[i] > tablica[Lewe(i)]) && (tablica[i] > tablica[Prawe(i)]))	// jesli wartosc ojca jest najwieksza, konczy
			return 0;
		
		if((tablica[i] < tablica[Lewe(i)]) && (tablica[Lewe(i)] > tablica[Prawe(i)]))
		{
			Zamien(tablica[i], tablica[Lewe(i)]);
			Sprawdz_galaz(Lewe(i));
			return 1;
		}
		
		if((tablica[i] < tablica[Prawe(i)]) && (tablica[Prawe(i)] > tablica[Lewe(i)]))
		{
			Zamien(tablica[i], tablica[Prawe(i)]);
			Sprawdz_galaz(Prawe(i));
			return 1;
		}
	}
	//std::cout << "ser" << std::endl;
	return 2;
	*/
	
		if(tablica[i] < tablica[Lewe(i)])		// jesli lewe wieksze od ojca
		{
			if(tablica[Lewe(i)] >= tablica[Prawe(i)])
			{
				Zamien(tablica[i], tablica[Lewe(i)]);
				Sprawdz_galaz(Lewe(i));
				return 0;
			}
			
			else
			{
				Zamien(tablica[i], tablica[Prawe(i)]);
				Sprawdz_galaz(Prawe(i));
				return 0;
			}
		}
		
		if(tablica[i] < tablica[Prawe(i)])		// jesli prawe wieksze od ojca
		{
			if(tablica[Prawe(i)] >= tablica[Lewe(i)])
			{
				Zamien(tablica[i], tablica[Prawe(i)]);
				Sprawdz_galaz(Prawe(i));
				return 0;
			}
			
			else
			{
				Zamien(tablica[i], tablica[Lewe(i)]);
				Sprawdz_galaz(Lewe(i));
				return 0;
			}
		}
		
		else 		// ojcie najwiekszy (lub rowny dzieciom)
		{
			return 1;
		}
		
	}
	
	return 2;		// wyszlo poza zakres tablicy
}

int Tablica::Ojciec(int i)
{
	if(i==0)
		return i;
	else if(i%2)
		return (i-1)/2;
	else
		return (i-2)/2;
}

int Tablica::Lewe(int i)
{
	return 2*i+1;
}

int Tablica::Prawe(int i)
{
	return 2*i+2;
}
/**********************************************************/


/*******************	BUBBLESORT	************************/

Tablica Tablica::Bubblesort()const
{
	Tablica s=*this;
	std::cout<<"Bubblesort"<<std::endl;
	
	s.Bulbul(s.rozmiar());
	
	return s;
}

void Tablica::Bulbul(int wielkosc)
{
	for(int i = 0; i < wielkosc; i++)
	{
		for(int j = 0; j < wielkosc-1; j++)
		{
			if(tablica[j] > tablica[j+1])
				Zamien(tablica[j], tablica[j+1]);
		}
	}	
}

/******************************************************/


class SumTester : public Tester<Tablica, Tablica>
{
	public:
		char a;
		SumTester(char i[]){a = i[0];};
		
	protected:
		Tablica runAlgorithm(const Tablica& inputData) override;
		Tablica readSingleInput(std::istream& inputStream) override;
};

Tablica SumTester::runAlgorithm(const Tablica& inputData)
{
    Tablica tab(inputData);
    
	switch( this->a)
		{
		case 'q':
			tab = tab.Quicksort();
			break;
		   
		case 'h':
			tab = tab.Heapsort();
			break;
		   
		case 'b':
			tab = tab.Bubblesort();
			break;
		}
		
    return tab;
}

Tablica SumTester::readSingleInput(std::istream& inputStream)
{
	  int rozmiar = 0;
	  inputStream >> rozmiar;
	  Tablica tablica(rozmiar);
	  inputStream >> tablica;
	  return tablica;
}

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		std::cout << "Podaj odpowednie argumenty wywolania" << std::endl;
		std::cout << "1. Nazwe pliku z wczytywanymi danymi" << std::endl;
		std::cout << "2. rodzaj sortowania ('h' - heapsort, 'q'- quicksort, 'b' - bubblesort" << std::endl;
		std::cout << std::endl;
	}
	
	else
	{	
		std::ifstream inputFile(argv[1]);
		
		std::ofstream dataOutputFile{"output.txt"}, timeOutputFile{"times.csv"};

		SumTester tester(argv[2]);

		if(!inputFile)
		{
			spdlog::error("input.txt cannot be opened!");
			return -1;
		}

		tester.runAllTests(inputFile, dataOutputFile, timeOutputFile);	
			
	}
	

}
